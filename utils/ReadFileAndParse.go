package utils

import (
	"fmt"
	"llprechner/models"
	"strconv"
	"strings"
)

func ReadFileAndParse(fileContent string) models.WeatherDataList {
	var weatherDataList []models.WeatherData

	lines := strings.Split(fileContent, "\n")

	for _, line := range lines {
		if !strings.HasPrefix(line, "STATIONS_ID") {
			data := strings.Split(line, ";")
			if len(data) < 17 {
				fmt.Printf("Error while parsing line: %s\n", line)
				continue
			}

			stationsID, _ := strconv.Atoi(strings.TrimSpace(data[0]))
			messDatum, _ := strconv.Atoi(strings.TrimSpace(data[1]))
			tm, _ := strconv.ParseFloat(strings.TrimSpace(data[13]), 64)
			// Parse other fields accordingly

			weatherData := models.WeatherData{
				STATIONSID:       stationsID,
				MessDatum:        messDatum,
				TagesMittelwerte: float64(tm),
			}

			weatherDataList = append(weatherDataList, weatherData)
		}
	}

	return weatherDataList
}
