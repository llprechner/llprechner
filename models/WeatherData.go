package models

import (
	"gorm.io/gorm"
)

type WeatherDataRepository struct {
	Db *gorm.DB
}

type WeatherDataList []WeatherData

type WeatherData struct {
	gorm.Model
	STATIONSID       int
	MessDatum        int
	TagesMittelwerte float64
}
