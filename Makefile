dev:
	air -c .air.toml

build:
	go build -o ./dist/main .
migrate:
	docker compose up -d migrate
start:
	chmod +x ./dist/main
	./dist/main

test:
	mkdir -p coverage
	go test -v -coverprofile ./coverage/cover.out ./...
	go tool cover -html=./coverage/cover.out -o ./coverage/cover.html
	open ./coverage/cover.html
docker_start_prod:
	docker compose --env-file .env.production -p llprechner-production -f docker-compose.yml -f docker-compose.production.yml up --build -d
docker_start_dev:
	docker compose --env-file .env -p llprechner-development -f docker-compose.yml -f docker-compose.development.yml up --build -d
docker_start_staging:
	docker compose --env-file .env.staging  -p llprechner-staging -f docker-compose.yml -f docker-compose.staging.yml up -d --build -d