package controllers

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"llprechner/models"
	"llprechner/utils"
	"net/http"
	"os"
)

func (c *Controller) Login(w http.ResponseWriter, r *http.Request) {
	var existingUser models.User
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	result := c.Db.Where("email = ?", r.FormValue("email")).First(&existingUser)
	if result.Error != nil {
		http.Error(w, "User does not exists", http.StatusUnauthorized)
		return
	}
	if existingUser.ID == 0 {
		http.Error(w, "User does not exists", http.StatusUnauthorized)
		return
	}
	errHash := utils.CompareHashPassword(r.FormValue("password"), existingUser.Password)
	if !errHash {
		http.Error(w, "invalid password", http.StatusUnauthorized)
		return
	}
	expirationTime := utils.GetExpirationTime()
	claims := &models.Claims{
		Role: existingUser.Role,
		StandardClaims: jwt.StandardClaims{
			Subject:   existingUser.Email,
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	jwtKey := []byte(os.Getenv("KEY"))
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		panic(err)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:     "token",
		Value:    tokenString,
		Expires:  expirationTime,
		Path:     "/",
		Domain:   "localhost",
		Secure:   false,
		HttpOnly: true,
	})
	fmt.Fprintf(w, "login successful")

}
func (c *Controller) LoginTemplate(w http.ResponseWriter, r *http.Request) {
	err := c.T.ExecuteTemplate(w, "login", nil)
	if err != nil {
		http.Error(w, "error", http.StatusInternalServerError)
		return
	}
}
