package controllers

import (
	"fmt"
	"io"
	"llprechner/utils"
	"mime/multipart"
	"net/http"
)

type WeatherPageStruct struct {
	IsAdmin bool
}

func (c *Controller) ImportWeatherData(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("token")
	if err != nil {
		http.Error(w, "error", http.StatusUnauthorized)
		return
	}
	claims, err := utils.ParseToken(cookie.Value)
	if err != nil {
		http.Error(w, "error", http.StatusUnauthorized)
		return
	}
	if claims.Role != "admin" {
		http.Error(w, "error", http.StatusUnauthorized)
		return
	}
	// Parse the multipart form data
	err = r.ParseMultipartForm(10 << 20) // Set a reasonable max memory

	if err != nil {
		http.Error(w, "Unable to parse form", http.StatusBadRequest)
		return
	}

	// Retrieve the form value and file
	fileDescription := r.FormValue("description")
	file, _, err := r.FormFile("file")

	if err != nil {
		http.Error(w, "Unable to retrieve file", http.StatusBadRequest)
		return
	}
	defer func(file multipart.File) {
		err := file.Close()
		if err != nil {
			http.Error(w, "Unable to close file", http.StatusInternalServerError)
			return
		}
	}(file)

	// Read and parse the file content
	fileContent, err := io.ReadAll(file)
	if err != nil {
		http.Error(w, "Error reading file", http.StatusInternalServerError)
		return
	}

	weatherDataList := utils.ReadFileAndParse(string(fileContent))
	c.Db.Create(&weatherDataList)
	// Respond with a success message
	message := fmt.Sprintf("%s is uploaded and processed", fileDescription)
	_, err = w.Write([]byte(message))
	if err != nil {
		http.Error(w, "Error writing response", http.StatusInternalServerError)
		return
	}
}
func (c *Controller) ImportWeatherDataTemplate(w http.ResponseWriter, r *http.Request) {
	pageStruct := WeatherPageStruct{IsAdmin: false}
	cookie, err := r.Cookie("token")

	if err != nil {
		c.T.ExecuteTemplate(w, "weather", pageStruct)
		return
	}
	claims, err := utils.ParseToken(cookie.Value)
	if err != nil {
		http.Error(w, "error", http.StatusUnauthorized)
		return
	}
	if claims.Role != "admin" {
		http.Error(w, "error", http.StatusUnauthorized)
		return
	}
	pageStruct.IsAdmin = true
	err = c.T.ExecuteTemplate(w, "weather", pageStruct)
	if err != nil {
		http.Error(w, "error", http.StatusInternalServerError)
		return
	}
}
