package controllers

import (
	"gorm.io/gorm"
	"html/template"
)

type Controller struct {
	Db *gorm.DB
	T  *template.Template
}
