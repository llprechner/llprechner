-- Create "weather_data" table
CREATE TABLE `weather_data` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(3) NULL,
  `updated_at` datetime(3) NULL,
  `deleted_at` datetime(3) NULL,
  `stations_id` bigint NULL,
  `mess_datum` bigint NULL,
  `tages_mittelwerte` double NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_weather_data_deleted_at` (`deleted_at`)
) CHARSET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
