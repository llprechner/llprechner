package main

import (
	"flag"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"html/template"
	"llprechner/controllers"
	"llprechner/models"
	"llprechner/routes"
	"llprechner/utils"
	"net/http"
	"os"
	"path/filepath"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		panic(err)
	} else {
		println("Environment variables loaded")
	}
	db, err := InitDB()
	if err != nil {
		panic(err)
	} else {
		println("DB connection established")
	}
	adminUser := os.Getenv("ADMIN_USER")
	adminPassword, _ := utils.GenerateHash(os.Getenv("ADMIN_PASSWORD"))
	adminEmail := os.Getenv("ADMIN_EMAIL")
	user := models.User{Name: adminUser, Password: adminPassword, Email: adminEmail, Role: "admin"}
	db.Create(&user)
	tmpl := template.Must(
		template.ParseFiles(
			filepath.Join("templates", "import", "weather.html"),
			filepath.Join("templates", "auth", "login.html"),
		))
	r := mux.NewRouter()
	controller := controllers.Controller{Db: db, T: tmpl}
	routing := routes.Routes{Controller: &controller, Router: r}
	var dir string
	flag.StringVar(&dir, "dir", "./static/", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()

	routing.AuthRoutes()
	routing.WeatherDataRoutes()
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))
	err = http.ListenAndServe(":"+os.Getenv("SERVER_PORT"), r)
	if err != nil {
		panic(err)
	} else {
		println("Server running on port" + os.Getenv("SERVER_PORT"))
	}

}

func InitDB() (*gorm.DB, error) {
	dsn := os.Getenv("DB_USER") + ":" + os.Getenv("DB_PASSWORD") + "@tcp(" + os.Getenv("DB_HOST") + ")/" + os.Getenv("DB_NAME") + "?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	Db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       dsn,
		DefaultStringSize:         256,   // default size for string fields
		DisableDatetimePrecision:  true,  // disable datetime precision, which not supported before MySQL 5.6
		DontSupportRenameIndex:    true,  // drop & create when rename index, rename index not supported before MySQL 5.7, MariaDB
		DontSupportRenameColumn:   true,  // `change` when rename column, rename column not supported before MySQL 8, MariaDB
		SkipInitializeWithVersion: false, // auto configure based on currently MySQL version
	}), &gorm.Config{CreateBatchSize: 1000})
	return Db, err
}
