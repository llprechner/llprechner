package routes

import (
	"github.com/gorilla/mux"
	"llprechner/controllers"
)

type Routes struct {
	Controller *controllers.Controller
	Router     *mux.Router
}
