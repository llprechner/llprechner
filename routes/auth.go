package routes

func (routes *Routes) AuthRoutes() {
	s := routes.Router.PathPrefix("/auth").Subrouter()
	s.HandleFunc("/login", routes.Controller.Login).Methods("POST")
	s.HandleFunc("/login", routes.Controller.LoginTemplate).Methods("GET")
}
