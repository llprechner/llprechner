package routes

func (routes *Routes) WeatherDataRoutes() {
	routes.Router.HandleFunc("/import/weather", routes.Controller.ImportWeatherData).Methods("POST")
	routes.Router.HandleFunc("/import/weather", routes.Controller.ImportWeatherDataTemplate).Methods("GET")
}
