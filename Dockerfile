# Development stage
FROM golang:1.21-alpine AS development

RUN apk add --no-cache git make gcc libc-dev

WORKDIR /usr/src/app

COPY go.mod go.sum .air.toml .env ./
RUN go install github.com/cosmtrek/air@latest
RUN go mod download
COPY .. .
EXPOSE 3000
CMD ["make", "dev"]

# Build stage
FROM golang:1.21-alpine AS build
RUN apk add --no-cache make
WORKDIR /usr/src/app
COPY --from=development /usr/src/app .
COPY --from=development /usr/src/app/templates .
RUN make build
# Final stage
FROM golang:1.21-alpine AS final
RUN apk add --no-cache make
WORKDIR /usr/src/app
COPY --from=build /usr/src/app/.air.toml ./air.toml
COPY --from=build /usr/src/app/dist ./dist
COPY --from=build /usr/src/app/migrations ./migrations
COPY --from=build /usr/src/app/templates ./templates
COPY .env .env
COPY --from=build /usr/src/app/Makefile ./Makefile

EXPOSE 3000
CMD ["make", "start"]
